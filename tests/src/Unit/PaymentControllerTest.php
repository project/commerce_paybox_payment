<?php

namespace Drupal\Tests\commerce_paybox_payment\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Tests\UnitTestCase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\commerce_paybox_payment\Controller\PaymentController;
use Drupal\commerce_paybox_payment\Services\PbxCmdRefHelper;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PaymentController Test.
 */
class PaymentControllerTest extends UnitTestCase {

  /**
   * Payment Controller.
   *
   * @var \Drupal\commerce_paybox_payment\Controller\PaymentController
   */
  private $paymentController;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $container = new ContainerBuilder();
    $orderType = $this->getMockBuilder(OrderTypeInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entityStorage = $this->getMockBuilder(EntityStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entityTypeManager = $this->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entityTypeManager->expects($this->any())
      ->method('getstorage')
      ->willReturn($entityStorage);
    $container->set('entity_type.manager', $entityTypeManager);
    $logger = $this->getMockBuilder(LoggerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $loggerChannelFactory = $this->getMockBuilder(LoggerChannelFactory::class)
      ->disableOriginalConstructor()
      ->getMock();
    $loggerChannelFactory->expects($this->any())
      ->method('get')
      ->willReturn($logger);
    $container->set('logger.factory', $loggerChannelFactory);
    $pbxCmdRefHelper = $this->getMockBuilder(PbxCmdRefHelper::class)
      ->disableOriginalConstructor()
      ->getMock();
    $pbxCmdRefHelper->expects($this->any())
      ->method('extractPaymentIdFromRef')
      ->willReturn('1');
    $container->set('commerce_paybox_payment.pbx_cmd_ref_helper', $pbxCmdRefHelper);
    $commercePriceRounder = $this->getMockBuilder(RounderInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $container->set('commerce_price.rounder', $commercePriceRounder);
    \Drupal::setContainer($container);

    $payment = $this->createPayment('12');

    $entityStorage->expects($this->any())
      ->method('load')
      ->will($this->returnValueMap([
        ['1', $payment],
        ['default', $orderType],
      ]));

    $this->paymentController = PaymentController::create($container);
  }

  /**
   * Test Return Payment Controller Method.
   *
   * @dataProvider providerValues
   */
  public function test(array $request, bool $assert) {
    $return = $this->paymentController->return(new Request($request));
    $this->assertEquals(new JsonResponse($assert), $return);
  }

  /**
   * Provider function.
   *
   * @return array[]
   *   Values.
   */
  public function providerValues() {
    return [
      [
        ['Ref' => 1, 'Mt' => '1300', 'Error' => PaymentController::EMPTY_ERROR],
        FALSE,
      ],
      [
        ['Ref' => 1, 'Mt' => '1200', 'Error' => '00001'],
        FALSE,
      ],
      [
        ['Ref' => 1, 'Mt' => '1200', 'Error' => PaymentController::EMPTY_ERROR],
        TRUE,
      ],
    ];
  }

  /**
   * Create fake payment entity.
   *
   * @param string $amount
   *   Amount number.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   Fake entity.
   */
  private function createPayment(string $amount) {
    $price = new Price($amount, 'EUR');
    $order = $this->getMockBuilder(OrderInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $order->expects($this->any())
      ->method('bundle')
      ->willReturn('default');
    $payment = $this->getMockBuilder(PaymentInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $payment->expects($this->any())
      ->method('getAmount')
      ->willReturn($price);
    $payment->expects($this->any())
      ->method('getOrder')
      ->willReturn($order);
    $state = $this->getMockBuilder(StateItemInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $state->expects($this->any())
      ->method('getId')
      ->willReturn('new');
    $payment->expects($this->any())
      ->method('getState')
      ->willReturn($state);

    return $payment;
  }

}
