CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Paybox integration for the Drupal Commerce payment and checkout system.
This module carries part of the functionality of the module on Drupal 8/9,
Paybox Service method using Commerce Payment Gateway is implemented here.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/commerce_paybox_payment

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/commerce_paybox_payment


REQUIREMENTS
------------

Required modules

* [Drupal Commerce](https://www.drupal.org/project/commerce)
* [Commerce Payment](https://www.drupal.org/project/commerce)

Paybox Service specific requirements
* PHP OpenSSL must be enabled on your server
(see documentation for how to enable it).


INSTALLATION
------------

* Install Commerce module
* Install Commerce Paybox Payment Module


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Commerce > Configuration > Payment > Payment Gateways
    3. Add payment gateway
    4. Choice Paybox Plugin and save plugin.
    5. Testing Data can be found in bank documentation
       (French doc available only)

Regarding the **Entity ID configuration** (mapping and separator),
config is stored globally.
It means that if you create a site with 2 or more Paybox gateways,
they will have the same config for those two specific values.

MAINTAINERS
-----------

* Anthony Delgado (anthonychoosit) - https://www.drupal.org/u/anthonychoosit

Supporting organizations:

* Spiriit - https://www.drupal.org/spiriit
