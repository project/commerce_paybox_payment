(function ($, Drupal) {
  Drupal.behaviors.adminPayboxPaymentAmount = {
    attach(context) {
      // Helper function to set or replace an URL param.
      function replaceUrlParam(url, paramName, paramValue) {
        if (paramValue == null) {
          paramValue = '';
        }
        const pattern = new RegExp(`\\b(${paramName}=).*?(&|#|$)`);
        if (url.search(pattern) >= 0) {
          return url.replace(pattern, `$1${paramValue}$2`);
        }
        url = url.replace(/[?#]$/, '');
        return `${
          url + (url.indexOf('?') > 0 ? '&' : '?') + paramName
        }=${paramValue}`;
      }

      // Useful variables.
      const $paymentForm = $('form#commerce-payment-add-form', context);
      const $amountField = $paymentForm.find(
        'input#edit-payment-amount-number',
      );
      const $redirectoFormLink = $paymentForm.find(
        'a#goto-paybox-payment-redirect-form',
      );
      const $defaultSubmitButton = $paymentForm.find(
        'input#edit-actions-submit',
      );

      // Hide default submit button.
      $defaultSubmitButton.hide();

      // If amount field get updated, add a query param to the redirect form link.
      $amountField.on('input', function () {
        $redirectoFormLink.attr(
          'href',
          replaceUrlParam(
            $redirectoFormLink.attr('href'),
            'amount',
            $(this).val(),
          ),
        );
      });
    },
  };
})(jQuery, Drupal);
