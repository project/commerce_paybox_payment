<?php

namespace Drupal\commerce_paybox_payment\Plugin\Commerce\PaymentGateway;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_paybox_payment\Controller\PaymentController;
use Drupal\commerce_paybox_payment\Services\PayboxDirectApiServiceInterface;
use Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface;
use Drupal\commerce_paybox_payment\SignatureChecker;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Paybox Commerce Payment Gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paybox",
 *   label = @Translation("Paybox"),
 *   display_label = @Translation("Paybox"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_paybox_payment\PluginForm\RedirectPayboxForm",
 *     "add-payment" = "Drupal\commerce_paybox_payment\PluginForm\PayboxPaymentAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 * )
 */
final class Paybox extends OffsitePaymentGatewayBase implements SupportsRefundsInterface {
  use LoggerChannelTrait;

  private const SERVICES = [
    'paybox' => 'Paybox (by Verifone)',
    'up2pay' => 'Up2pay (by Crédit Agricole)',
  ];

  private const PROTOCOL_VERSIONS = [
    '00103' => 'Paybox Direct / Up2pay e-transactions Access',
    '00104' => 'Paybox Direct Plus / Up2pay e-transactions Premium',
  ];

  private const CURRENCY_CODE_MAPPING = [
    '978' => 'EUR',
    '840' => 'USD',
    '826' => 'GBP',
    '032' => 'ARS',
    '040' => 'ATS',
    '036' => 'AUD',
    '056' => 'BEF',
    '986' => 'BRL',
    '124' => 'CAD',
    '756' => 'CHF',
    '280' => 'DEM',
    '208' => 'DKK',
    '246' => 'FIM',
    '300' => 'GRD',
    '392' => 'JPY',
    '116' => 'KHR',
    '410' => 'KRW',
    '484' => 'MXN',
    '578' => 'NOK',
    '554' => 'NZD',
    '752' => 'SEK',
    '702' => 'SGD',
    '792' => 'TRL',
    '901' => 'TWD',
  ];

  private const LANGUAGE_CODE_MAPPING = [
    'fr' => 'fra',
    'it' => 'ita',
    'sv' => 'swe',
    'en' => 'gbr',
    'de' => 'deu',
    'pt-pt' => 'prt',
    'es' => 'esp',
    'nl' => 'nld',
  ];

  private const ENTITY_ID_SEPARATOR = [
    '-' => '-',
    '_' => '_',
  ];

  private const WORLD_BANK_MAPPING = [
    'AD' => '020',
    'AE' => '784',
    'AF' => '004',
    'AG' => '028',
    'AI' => '660',
    'AL' => '008',
    'AM' => '051',
    'AO' => '024',
    'AQ' => '010',
    'AR' => '032',
    'AS' => '016',
    'AT' => '040',
    'AU' => '036',
    'AW' => '533',
    'AX' => '248',
    'AZ' => '031',
    'BA' => '070',
    'BB' => '052',
    'BD' => '050',
    'BE' => '056',
    'BF' => '854',
    'BG' => '100',
    'BH' => '048',
    'BI' => '108',
    'BJ' => '204',
    'BL' => '652',
    'BM' => '060',
    'BN' => '096',
    'BO' => '068',
    'BQ' => '535',
    'BR' => '076',
    'BS' => '044',
    'BT' => '064',
    'BV' => '074',
    'BW' => '072',
    'BY' => '112',
    'BZ' => '084',
    'CA' => '124',
    'CC' => '166',
    'CD' => '180',
    'CF' => '140',
    'CG' => '178',
    'CH' => '756',
    'CI' => '384',
    'CK' => '184',
    'CL' => '152',
    'CM' => '120',
    'CN' => '156',
    'CO' => '170',
    'CR' => '188',
    'CU' => '192',
    'CV' => '132',
    'CW' => '531',
    'CX' => '162',
    'CY' => '196',
    'CZ' => '203',
    'DE' => '276',
    'DJ' => '262',
    'DK' => '208',
    'DM' => '212',
    'DO' => '214',
    'DZ' => '012',
    'EC' => '218',
    'EE' => '233',
    'EG' => '818',
    'EH' => '732',
    'ER' => '232',
    'ES' => '724',
    'ET' => '231',
    'FI' => '246',
    'FJ' => '242',
    'FK' => '238',
    'FM' => '583',
    'FO' => '234',
    'FR' => '250',
    'GA' => '266',
    'GB' => '826',
    'GD' => '308',
    'GE' => '268',
    'GF' => '254',
    'GG' => '831',
    'GH' => '288',
    'GI' => '292',
    'GL' => '304',
    'GM' => '270',
    'GN' => '324',
    'GP' => '312',
    'GQ' => '226',
    'GR' => '300',
    'GS' => '239',
    'GT' => '320',
    'GU' => '316',
    'GW' => '624',
    'GY' => '328',
    'HK' => '344',
    'HM' => '334',
    'HN' => '340',
    'HR' => '191',
    'HT' => '332',
    'HU' => '348',
    'ID' => '360',
    'IE' => '372',
    'IL' => '376',
    'IM' => '833',
    'IN' => '356',
    'IO' => '086',
    'IQ' => '368',
    'IR' => '364',
    'IS' => '352',
    'IT' => '380',
    'JE' => '832',
    'JM' => '388',
    'JO' => '400',
    'JP' => '392',
    'KE' => '404',
    'KG' => '417',
    'KH' => '116',
    'KI' => '296',
    'KM' => '174',
    'KN' => '659',
    'KP' => '408',
    'KR' => '410',
    'KW' => '414',
    'KY' => '136',
    'KZ' => '398',
    'LA' => '418',
    'LB' => '422',
    'LC' => '662',
    'LI' => '438',
    'LK' => '144',
    'LR' => '430',
    'LS' => '426',
    'LT' => '440',
    'LU' => '442',
    'LV' => '428',
    'LY' => '434',
    'MA' => '504',
    'MC' => '492',
    'MD' => '498',
    'ME' => '499',
    'MF' => '663',
    'MG' => '450',
    'MH' => '584',
    'MK' => '807',
    'ML' => '466',
    'MM' => '104',
    'MN' => '496',
    'MO' => '446',
    'MP' => '580',
    'MQ' => '474',
    'MR' => '478',
    'MS' => '500',
    'MT' => '470',
    'MU' => '480',
    'MV' => '462',
    'MW' => '454',
    'MX' => '484',
    'MY' => '458',
    'MZ' => '508',
    'NA' => '516',
    'NC' => '540',
    'NE' => '562',
    'NF' => '574',
    'NG' => '566',
    'NI' => '558',
    'NL' => '528',
    'NO' => '578',
    'NP' => '524',
    'NR' => '520',
    'NU' => '570',
    'NZ' => '554',
    'OM' => '512',
    'PA' => '591',
    'PE' => '604',
    'PF' => '258',
    'PG' => '598',
    'PH' => '608',
    'PK' => '586',
    'PL' => '616',
    'PM' => '666',
    'PN' => '612',
    'PR' => '630',
    'PS' => '275',
    'PT' => '620',
    'PW' => '585',
    'PY' => '600',
    'QA' => '634',
    'RE' => '638',
    'RO' => '642',
    'RS' => '688',
    'RU' => '643',
    'RW' => '646',
    'SA' => '682',
    'SB' => '090',
    'SC' => '690',
    'SD' => '729',
    'SE' => '752',
    'SG' => '702',
    'SH' => '654',
    'SI' => '705',
    'SJ' => '744',
    'SK' => '703',
    'SL' => '694',
    'SM' => '674',
    'SN' => '686',
    'SO' => '706',
    'SR' => '740',
    'SS' => '728',
    'ST' => '678',
    'SV' => '222',
    'SX' => '534',
    'SY' => '760',
    'SZ' => '748',
    'TC' => '796',
    'TD' => '148',
    'TF' => '260',
    'TG' => '768',
    'TH' => '764',
    'TJ' => '762',
    'TK' => '772',
    'TL' => '626',
    'TM' => '795',
    'TN' => '788',
    'TO' => '776',
    'TR' => '792',
    'TT' => '780',
    'TV' => '798',
    'TW' => '158',
    'TZ' => '834',
    'UA' => '804',
    'UG' => '800',
    'UM' => '581',
    'US' => '840',
    'UY' => '858',
    'UZ' => '860',
    'VA' => '336',
    'VC' => '670',
    'VE' => '862',
    'VG' => '092',
    'VI' => '850',
    'VN' => '704',
    'VU' => '548',
    'WF' => '876',
    'WS' => '882',
    'YE' => '887',
    'YT' => '175',
    'ZA' => '710',
    'ZM' => '894',
    'ZW' => '716',
  ];

  /**
   * Psr\Log\LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * LanguageManager Service.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Drupal\Core\Config\Config Service.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $globalConfig;

  /**
   * Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface Service.
   *
   * @var \Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface
   */
  protected $pbxCmdRefHelper;

  /**
   * Drupal\commerce_paybox_payment\Services\PayboxDirectApiService Service.
   *
   * @var \Drupal\commerce_paybox_payment\Services\PayboxDirectApiServiceInterface
   */
  protected $api;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Paybox constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   Payment Type Manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   Payment Method Type Manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time Service.
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   LanguageManager Service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The Config Factory Service.
   * @param \Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface $pbxCmdRefHelper
   *   The Paybox Command Ref Helper Service.
   * @param \Drupal\commerce_paybox_payment\Services\PayboxDirectApiServiceInterface $payboxDirectApiService
   *   The Paybox Direct API Service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    LanguageManager $languageManager,
    ConfigFactory $configFactory,
    PbxCmdRefHelperInterface $pbxCmdRefHelper,
    PayboxDirectApiServiceInterface $payboxDirectApiService,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->logger = $this->getLogger('commerce_paybox_payment');
    $this->languageManager = $languageManager;
    $this->globalConfig = $configFactory->getEditable('commerce_paybox_payment.settings');
    $this->pbxCmdRefHelper = $pbxCmdRefHelper;
    $this->api = $payboxDirectApiService;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('language_manager'),
      $container->get('config.factory'),
      $container->get('commerce_paybox_payment.pbx_cmd_ref_helper'),
      $container->get('commerce_paybox_payment.paybox_direct_api'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'service' => '',
      'protocol_version' => '',
      'merchant_id' => '',
      'rank' => '',
      'login' => '',
      'secret_key' => '',
      'server_url' => '',
      'currency_code' => '',
      'payment_diff' => '',
      'entity_id_mapping' => 'payment_id',
      'entity_id_separator' => '_',
      'use_secondary_ppps_endpoint' => FALSE,
      'user_firstname_field' => '',
      'user_lastname_field' => '',
      'user_phone_field' => '',
      'default_phone' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['service'] = [
      '#type' => 'select',
      '#title' => $this->t('Service'),
      '#options' => self::SERVICES,
      '#default_value' => $this->configuration['service'],
      '#required' => TRUE,
    ];
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site'),
      '#size' => 20,
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['rank'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rank'),
      '#size' => 20,
      '#default_value' => $this->configuration['rank'],
      '#required' => TRUE,
    ];
    $form['login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login'),
      '#size' => 20,
      '#default_value' => $this->configuration['login'],
      '#required' => TRUE,
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HMAC Secret key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];
    $form['server_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The Paybox payment server URL'),
      '#description' => $this->t('The absolute URL to the Paybox payment server to process transactions.'),
      '#size' => 100,
      '#default_value' => $this->configuration['server_url'],
      '#required' => TRUE,
    ];
    $form['protocol_version'] = [
      '#type' => 'select',
      '#title' => $this->t('PPPS protocol version'),
      '#options' => self::PROTOCOL_VERSIONS,
      '#default_value' => $this->configuration['protocol_version'],
      '#required' => TRUE,
    ];
    $form['use_secondary_ppps_endpoint'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use secondary PPPS endpoint'),
      '#description' => $this->t('Production server provides two endpoint for API endpoint, primary and secondary.') .
      '<br />' . $this->t('Any doubts, use primary (by not checking this box).'),
      '#default_value' => $this->configuration['use_secondary_ppps_endpoint'],
    ];
    $form['currency_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#options' => self::CURRENCY_CODE_MAPPING,
      '#default_value' => $this->configuration['currency_code'],
    ];
    $form['payment_diff'] = [
      '#type' => 'textfield',
      '#attributes' => [
        ' type' => 'number',
      ],
      '#title' => $this->t('Deferred payment'),
      '#default_value' => $this->configuration['payment_diff'],
    ];
    $form['entity_id_mapping'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity ID mapping'),
      '#description' => $this->t('This will be displayed in the Paybox/Up2Pay backoffice.'),
      '#options' => [
        'payment_id' => $this->t('Payment ID'),
        'payment_and_order_ids' => $this->t('Payment and order IDs'),
      ],
      '#default_value' => $this->globalConfig->get('entity_id_mapping'),
      '#required' => TRUE,
    ];
    $form['entity_id_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Separator'),
      '#description' => $this->t('Make sure your order ID is not containing the separator, it could break the mapping.') .
      '<br />' . $this->t('Any doubts, please select <strong>Payment ID</strong> option.'),
      '#options' => self::ENTITY_ID_SEPARATOR,
      '#default_value' => $this->globalConfig->get('entity_id_separator'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="configuration[paybox][entity_id_mapping]"]' => [
            'value' => 'payment_and_order_ids',
          ],
        ],
      ],
    ];
    $fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $fields = array_filter($fields, function (FieldDefinitionInterface $field) {
      return 'string' === $field->getType();
    });
    $options = [];
    foreach ($fields as $field) {
      $options[$field->getName()] = $field->getLabel();
    }
    $form['user_firstname_field'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('User firstname field'),
      '#default_value' => $this->configuration['user_firstname_field'],
      '#required' => TRUE,
    ];
    $form['user_lastname_field'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('User lastname field'),
      '#default_value' => $this->configuration['user_lastname_field'],
      '#required' => TRUE,
    ];
    $form['user_phone_field'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('User phone field'),
      '#default_value' => $this->configuration['user_phone_field'],
      '#required' => TRUE,
    ];
    $form['default_phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default phone value'),
      '#default_value' => $this->configuration['default_phone'],
      '#required' => TRUE,
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['service'] = $values['service'];
    $this->configuration['merchant_id'] = $values['merchant_id'];
    $this->configuration['rank'] = $values['rank'];
    $this->configuration['login'] = $values['login'];
    $this->configuration['secret_key'] = $values['secret_key'];
    $this->configuration['server_url'] = $values['server_url'];
    $this->configuration['protocol_version'] = $values['protocol_version'];
    $this->configuration['use_secondary_ppps_endpoint'] = $values['use_secondary_ppps_endpoint'];
    $this->configuration['currency_code'] = $values['currency_code'];
    $this->configuration['payment_diff'] = $values['payment_diff'];
    $this->configuration['user_firstname_field'] = $values['user_firstname_field'];
    $this->configuration['user_lastname_field'] = $values['user_lastname_field'];
    $this->configuration['user_phone_field'] = $values['user_phone_field'];
    $this->configuration['default_phone'] = $values['default_phone'];

    $this->globalConfig->set('entity_id_mapping', $values['entity_id_mapping']);
    $this->globalConfig->set('entity_id_separator', $values['entity_id_separator']);
    $this->globalConfig->save();
  }

  /**
   * Construct Paybox request data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order entity.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   Payment entity.
   *
   * @return array
   *   Request data build.
   */
  public function request(OrderInterface $order, PaymentInterface $payment) {
    $plugin_config = $this->getConfiguration();

    // Save payment to have reference to pass to bank site.
    $payment->save();

    // IPN return callback URL (to override platform config).
    $ipn_return_url = Url::fromRoute('commerce_paybox_payment.payment_api_return', [
      'commerce_order' => $order->id(),
    ])->setAbsolute(TRUE)->toString();

    $paymentAmount = (float) $payment->getAmount()->getNumber() * 100;
    $data = [
      'PBX_SITE' => $plugin_config['merchant_id'],
      'PBX_RANG' => $plugin_config['rank'],
      'PBX_IDENTIFIANT' => $plugin_config['login'],
      'PBX_TOTAL' => str_pad((string) $paymentAmount, 3, '0', STR_PAD_LEFT),
      'PBX_DEVISE' => $plugin_config['currency_code'],
      'PBX_CMD' => $this->pbxCmdRefHelper->createPbxCmdValueFromPayment($payment),
      'PBX_PORTEUR' => $order->getEmail(),
      'PBX_RETOUR' => 'Mt:M;Ref:R;Transaction:S;Appel:T;Date:Q;Auto:A;Error:E;Signature:K',
      'PBX_HASH' => 'SHA512',
      'PBX_TIME' => date('c', $order->getCreatedTime()),
      'PBX_REPONDRE_A' => $ipn_return_url,
    ];

    // Add secure v2 params.
    $user = $order->getCustomer();
    $profiles = $order->collectProfiles();
    $profile = reset($profiles);
    $address = $profile->get('address')->first()->getValue();
    $xmlEncoder = new XmlEncoder();
    $pbx_shoppingcart = $xmlEncoder->encode([
      'total' => [
        'totalQuantity' => count($order->getItems()),
      ],
    ], 'xml', [
      'csv_end_of_line' => '',
      'xml_encoding' => 'utf-8',
      'xml_root_node_name' => 'shoppingcart',
    ]);
    // Add phone infos.
    $phone = $plugin_config['default_phone'];
    $phone_field = $plugin_config['user_phone_field'];
    if (
      $user->hasField($phone_field) &&
      !$user->get($phone_field)->isEmpty()
    ) {
      $phone = $user->get($phone_field)->getString();
    }
    $phone_util = PhoneNumberUtil::getInstance();
    $phone_proto = $phone_util->parse($phone, $address['country_code']);
    $pbs_billing = $xmlEncoder->encode([
      'Address' => [
        'FirstName' => $user->isAuthenticated() ? $user->get($plugin_config['user_firstname_field'])->getString() : $address['given_name'],
        'LastName' => $user->isAuthenticated() ? $user->get($plugin_config['user_lastname_field'])->getString() : $address['family_name'],
        'Address1' => $address['address_line1'],
        'ZipCode' => $address['postal_code'],
        'City' => $address['locality'],
        'CountryCode' => self::WORLD_BANK_MAPPING[$address['country_code']],
        'MobilePhone' => $phone_util->format($phone_proto, PhoneNumberFormat::NATIONAL),
        'CountryCodeMobilePhone' => sprintf('+%s', $phone_proto->getCountryCode()),
      ],
    ], 'xml', [
      'csv_end_of_line' => '',
      'xml_encoding' => 'utf-8',
      'xml_root_node_name' => 'Billing',
    ]);
    $data['PBX_SHOPPINGCART'] = str_replace("\n", '', $pbx_shoppingcart);
    $data['PBX_BILLING'] = str_replace("\n", '', $pbs_billing);

    $language = $this->languageManager->getCurrentLanguage()->getId();
    if (!empty(self::LANGUAGE_CODE_MAPPING[$language])) {
      $data['PBX_LANGUE'] = strtoupper(self::LANGUAGE_CODE_MAPPING[$language]);
    }

    if (!empty($plugin_config['payment_diff'])) {
      $data['PBX_DIFF'] = (int) $plugin_config['payment_diff'];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $paymentId = $this->pbxCmdRefHelper->extractPaymentIdFromRef($request);
    $payment = $this->entityTypeManager->getStorage('commerce_payment')->load($paymentId);

    if (!$payment instanceof PaymentInterface) {
      throw new PaymentGatewayException('Payment cannot be found');
    }

    if (1 !== SignatureChecker::checkSignature($request)) {
      throw new PaymentGatewayException('Invalid Signature');
    }

    if ('completed' === $payment->getState()->getId()) {
      throw new PaymentGatewayException('Payment is already completed');
    }

    $payment->setState('voided');
    $payment->save();
    parent::onCancel($order, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $paymentId = $this->pbxCmdRefHelper->extractPaymentIdFromRef($request);
    $transactionId = $request->query->get('Transaction');
    $appelId = $request->query->get('Appel');
    $transactionDate = $request->query->get('Date');
    $payment = $this->entityTypeManager->getStorage('commerce_payment')->load($paymentId);

    if (!$payment instanceof PaymentInterface) {
      throw new PaymentGatewayException('Payment cannot be found');
    }

    if (1 !== SignatureChecker::checkSignature($request)) {
      throw new PaymentGatewayException('Invalid Signature');
    }

    if ('completed' === $payment->getState()->getId()) {
      throw new PaymentGatewayException('Payment is already completed');
    }

    $returnAmount = $request->query->get('Mt');
    $paymentAmount = (float) $payment->getAmount()->getNumber() * 100;
    $paymentAmount = str_pad((string) $paymentAmount, 3, '0', STR_PAD_LEFT);
    $error = $request->query->get('Error');
    if ($returnAmount !== $paymentAmount) {
      throw new PaymentGatewayException('Charged amount not equal to order amount');
    }
    if (!empty($error) && PaymentController::EMPTY_ERROR !== $error  && PaymentController::PENDING_CODE !== $error) {
      throw new PaymentGatewayException('An error has occurred during payment');
    }

    if (PaymentController::PENDING_CODE === $error) {
      $payment->setState('pending');
      $payment->set('remote_id', $transactionId);
      $payment->save();
      parent::onReturn($order, $request);
      return;
    }

    $payment->setState('completed');
    $payment->set('remote_id', Json::encode([
      'TRANS' => $transactionId,
      'APPEL' => $appelId,
    ]));
    $payment->set('authorized', strtotime('today ' . $transactionDate));
    $payment->save();
    parent::onReturn($order, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    // Only completed or partially refunded payments can be refunded.
    return in_array($payment->getState()->getId(), [
      'completed',
      'partially_refunded',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    try {
      $this->api->refund($payment, $amount);
    }
    catch (\Exception $e) {
      $this->logger->log('error', 'An error happened while calling Paybox API: ' . $e->getMessage());
      throw new PaymentGatewayException('An error happened while calling Paybox API');
    }

    // Determine whether payment has been fully or partially refunded.
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

}
