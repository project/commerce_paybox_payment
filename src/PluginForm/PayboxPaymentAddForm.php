<?php

namespace Drupal\commerce_paybox_payment\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;

/**
 * Form to add manually payment from back-office.
 */
class PayboxPaymentAddForm extends PaymentGatewayFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    if (!$order) {
      throw new \InvalidArgumentException('Payment entity with no order reference given to PayboxPaymentAddForm.');
    }

    // The payment amount should not exceed the remaining order balance.
    $balance = $order->getBalance();
    $amount = $balance->isPositive() ? $balance : $balance->multiply('0');

    // Save payment to get an ID.
    $payment->setAmount($amount);
    $payment->save();

    $form['amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Amount'),
      '#default_value' => $amount->toArray(),
      '#required' => TRUE,
    ];

    $form['redirect_form_link'] = [
      '#prefix' => '<div class="actions">',
      '#type' => 'link',
      '#title' => $this->t('Go to Paybox payment page'),
      '#attributes' => [
        'id' => 'goto-paybox-payment-redirect-form',
        'class' => [
          'button',
          'button--primary',
        ],
      ],
      '#url' => Url::fromRoute('commerce_paybox_payment.add_payment_redirect_form', [
        'commerce_order' => $order->id(),
        'commerce_payment' => $payment->id(),
      ]),
      '#suffix' => '</div>',
    ];

    $form['#attached']['library'][] = 'commerce_paybox_payment/admin_paybox_payment_amount';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

}
