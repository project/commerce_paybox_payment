<?php

namespace Drupal\commerce_paybox_payment\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;

/**
 * RedirectPaybox Form class.
 */
class RedirectPayboxForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();

    /** @var \Drupal\commerce_paybox_payment\Plugin\Commerce\PaymentGateway\Paybox $gateway_plugin */
    $gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $config = $gateway_plugin->getConfiguration();

    $data = $gateway_plugin->request($order, $payment);
    $data['PBX_EFFECTUE'] = $form['#return_url'];
    $data['PBX_ATTENTE'] = $form['#return_url'];
    $data['PBX_REPONDRE_A'] = Url::fromRoute('commerce_paybox_payment.payment_api_return', [
      'commerce_order' => $order->id(),
    ])->setAbsolute(TRUE)->toString();
    $data['PBX_ANNULE'] = $form['#cancel_url'];
    $data['PBX_REFUSE'] = $form['#cancel_url'];
    $data['PBX_HMAC'] = $this->getDataHmac($data, $config['secret_key']);

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $config['server_url'],
      $data,
      self::REDIRECT_POST
    );
  }

  /**
   * Get Hmac hash from array request.
   *
   * @param array $data
   *   Request data.
   * @param string $secretKey
   *   Secret key.
   *
   * @return string
   *   Hmac data request.
   */
  private function getDataHmac(array $data, string $secretKey) {
    // Converts the array into a string of key=value.
    $attached_data = [];
    foreach ($data as $key => $value) {
      $attached_data[] = $key . '=' . $value;
    }

    $msg = implode('&', $attached_data);
    $binKey = pack("H*", $secretKey);

    return strtoupper(hash_hmac('sha512', $msg, $binKey));
  }

}
