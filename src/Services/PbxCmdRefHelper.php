<?php

namespace Drupal\commerce_paybox_payment\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides methods to build/extract PBX_CMD parameter.
 */
class PbxCmdRefHelper implements PbxCmdRefHelperInterface {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Drupal\Core\Config\Config definition.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $globalConfig;

  /**
   * Constructs a new PbxCmdRefHelper object.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
    $this->globalConfig = $this->configFactory->getEditable('commerce_paybox_payment.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue($value) {
    return $this->globalConfig->get($value);
  }

  /**
   * {@inheritdoc}
   */
  public function createPbxCmdValueFromPayment(PaymentInterface $payment): string {

    // Keep using switch instead of match.
    // Because of PHP 7.4 support for Drupal 8.8.
    switch ($this->globalConfig->get('entity_id_mapping')) {
      case 'payment_and_order_ids':
        return implode($this->globalConfig->get('entity_id_separator'), [
          $payment->id(),
          $payment->getOrderId(),
        ]);

      case 'payment_id':
      default:
        return $payment->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function extractPaymentIdFromRef(Request $request): string {
    $merchant_ref_value = $request->query->get('Ref');

    // If Ref contains both payment and order ids, a small process is required.
    if ($this->globalConfig->get('entity_id_mapping') === 'payment_and_order_ids') {
      if ($exploded_ref = explode($this->globalConfig->get('entity_id_separator'), $merchant_ref_value)) {
        return $exploded_ref[0];
      }
    }

    // Default case, only payment id, nothing required.
    return $merchant_ref_value;
  }

}
