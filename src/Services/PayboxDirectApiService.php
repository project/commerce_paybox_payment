<?php

namespace Drupal\commerce_paybox_payment\Services;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use GuzzleHttp\Client;

/**
 * Class PayboxDirectApi Service.
 */
class PayboxDirectApiService implements PayboxDirectApiServiceInterface {
  use LoggerChannelTrait;

  const TRANSACTION_ENDPOINT = 'PPPS.php';
  const ACTIVITE = '024';
  const TYPE_REFUND = '00014';
  const HASH = 'SHA512';

  const PAYBOX_DIRECT_GATEWAY_TEST = 'https://preprod-ppps.paybox.com/';
  const PAYBOX_DIRECT_GATEWAY_PROD = 'https://ppps.paybox.com/';
  const PAYBOX_DIRECT_GATEWAY_PROD_SECONDARY = 'https://ppps1.paybox.com/';

  const UP2PAY_GAE_GATEWAY_TEST = 'https://recette-ppps.e-transactions.fr/';
  const UP2PAY_GAE_GATEWAY_PROD = 'https://ppps.e-transactions.fr/';
  const UP2PAY_GAE_GATEWAY_PROD_SECONDARY = 'https://ppps1.e-transactions.fr/';

  /**
   * Psr\Log\LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Drupal\Core\Config\Config Service.
   *
   * @var PbxCmdRefHelperInterface
   */
  private $pbxCmdRefHelper;

  /**
   * Array containing API parameters.
   *
   * @var array
   */
  private array $settings = [];

  /**
   * Constructs a new PayboxDirectApiService object.
   */
  public function __construct(PbxCmdRefHelperInterface $pbxCmdRefHelper) {
    $this->logger = $this->getLogger('commerce_paybox_payment');
    $this->pbxCmdRefHelper = $pbxCmdRefHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndPoint(array $plugin_config): string {
    switch ($plugin_config['service'] . '_' . $plugin_config['mode']) {
      case 'up2pay_live':
        $gateway = $plugin_config['use_secondary_ppps_endpoint'] ? self::UP2PAY_GAE_GATEWAY_PROD_SECONDARY : self::UP2PAY_GAE_GATEWAY_PROD;
        break;

      case 'up2pay_test':
        $gateway = self::UP2PAY_GAE_GATEWAY_TEST;
        break;

      case 'paybox_live':
        $gateway = $plugin_config['use_secondary_ppps_endpoint'] ? self::PAYBOX_DIRECT_GATEWAY_PROD_SECONDARY : self::PAYBOX_DIRECT_GATEWAY_PROD;
        break;

      case 'paybox_test':
      default:
        $gateway = self::PAYBOX_DIRECT_GATEWAY_TEST;
        break;
    }

    return $gateway . self::TRANSACTION_ENDPOINT;
  }

  /**
   * {@inheritdoc}
   */
  public function signWithHmacKey($plugin_config) {

    // Prepare message for HMAC signature.
    $msg = \http_build_query($this->settings);

    // Get HMAC key from gateway config.
    $key = $plugin_config['secret_key'];

    // Transform ASCII key into binary.
    $bin_key = pack("H*", $key);

    // Hash message and "upper" it.
    $hmac = strtoupper(hash_hmac($this->settings['HASH'], $msg, $bin_key));

    // Finally, add hashed message (signature) as "HMAC" setting.
    $this->settings['HMAC'] = $hmac;
  }

  /**
   * {@inheritdoc}
   */
  public function setBasicSettings(array $plugin_config) {
    $now = new \DateTime('now', new \DateTimeZone('Europe/Paris'));

    // Some constant values.
    $this->settings['ACTIVITE'] = self::ACTIVITE;
    $this->settings['HASH'] = self::HASH;

    // Values from gateway configuration.
    $this->settings['VERSION'] = $plugin_config['protocol_version'];
    $this->settings['SITE'] = $plugin_config['merchant_id'];
    $this->settings['RANG'] = $plugin_config['rank'];
    $this->settings['DEVISE'] = $plugin_config['merchant_id'];

    // Arbitrary & unique values, based on current date.
    $this->settings['NUMQUESTION'] = sprintf('%010d', $now->format('U'));
    $this->settings['DATEQ'] = $now->format('dmYHis');
  }

  /**
   * {@inheritdoc}
   */
  public function setRefundSettings(array $plugin_config, PaymentInterface $payment, Price $amount) {
    $this->settings['TYPE'] = self::TYPE_REFUND;

    // Format refunded amount according to Paybox/Up2Pay requirements.
    $number = (int) $amount->getNumber();
    $this->settings['MONTANT'] = sprintf('%010d', $number * 100);

    // Provide a reference for the refund transaction.
    // Based on the original payment transaction.
    $this->settings['REFERENCE'] = implode($this->pbxCmdRefHelper->getValue('entity_id_separator'), [
      'RBST', $this->pbxCmdRefHelper->createPbxCmdValueFromPayment($payment),
    ]);

    // Get these two params from the payment entity remote id.
    $remoteParams = Json::decode($payment->getRemoteId());
    $this->settings['NUMAPPEL'] = sprintf('%010d', $remoteParams['APPEL']);
    $this->settings['NUMTRANS'] = sprintf('%010d', $remoteParams['TRANS']);
  }

  /**
   * {@inheritdoc}
   */
  public function refund(PaymentInterface $payment, Price $amount) {
    $plugin_config = $payment->getPaymentGateway()->getPluginConfiguration();

    // Prepare API call.
    $endpoint = $this->getEndPoint($plugin_config);
    $this->setBasicSettings($plugin_config);
    $this->setRefundSettings($plugin_config, $payment, $amount);
    $this->signWithHmacKey($plugin_config);

    // Log API call settings.
    $debug = 'Action: <strong>refund</strong>';
    $debug .= '<br />Endpoint: ' . $endpoint;
    $debug .= '<br />Order ID: ' . $payment->getOrderId();
    $debug .= '<br />Params: <pre>' . print_r($this->getSettings(), TRUE) . '</pre>';
    $this->logger->debug($debug);

    // Make the call.
    $client = new Client();
    $client->request('POST', $endpoint, ['form_params' => $this->getSettings()]);
  }

}
