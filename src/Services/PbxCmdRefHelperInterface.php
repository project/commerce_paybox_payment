<?php

namespace Drupal\commerce_paybox_payment\Services;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * An interface for the PbxCmdRefHelper service.
 */
interface PbxCmdRefHelperInterface {

  /**
   * Return module config value.
   *
   * @param string $value
   *   Machine name config.
   *
   * @return mixed
   *   Config value.
   */
  public function getValue(string $value);

  /**
   * Provide value to send to Paybox/Up2Pay as "PBX_CMD" parameter.
   *
   * Based on module configuration.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   Payment entity.
   *
   * @return string
   *   Built param.
   */
  public function createPbxCmdValueFromPayment(PaymentInterface $payment): string;

  /**
   * Extract payment id from "Ref" parameter sent back from Paybox/Up2Pay.
   *
   * Based on module configuration.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request.
   *
   * @return string
   *   Payment id.
   */
  public function extractPaymentIdFromRef(Request $request): string;

}
