<?php

namespace Drupal\commerce_paybox_payment\Services;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;

/**
 * Interface to PayboxDirectApi Service.
 */
interface PayboxDirectApiServiceInterface {

  /**
   * Return API call parameters.
   *
   * @return array
   *   Array Parameters.
   */
  public function getSettings(): array;

  /**
   * Return API endpoint based on plugin config.
   *
   * @param array $plugin_config
   *   Plugin config.
   *
   * @return string
   *   Url endpoint.
   */
  public function getEndPoint(array $plugin_config): string;

  /**
   * Prepare API authentication.
   *
   * @param array $plugin_config
   *   Array Parameters.
   */
  public function signWithHmacKey(array $plugin_config);

  /**
   * Define global settings which would be the same for all API calls.
   *
   * See Paybox & Up2Pay documentations for more info.
   *
   * @param array $plugin_config
   *   The payment gateway plugin config.
   *
   * @throws \Exception
   */
  public function setBasicSettings(array $plugin_config);

  /**
   * Add specific settings for a refund transaction.
   *
   * @param array $plugin_config
   *   Array Parameters.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\commerce_price\Price $amount
   *   The amount to refund.
   */
  public function setRefundSettings(array $plugin_config, PaymentInterface $payment, Price $amount);

  /**
   * Refunds the given payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\commerce_price\Price $amount
   *   The amount to refund.
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function refund(PaymentInterface $payment, Price $amount);

}
