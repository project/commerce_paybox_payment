<?php

namespace Drupal\commerce_paybox_payment\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Manage Paybox Server Calls.
 */
final class PaymentController extends ControllerBase {

  const EMPTY_ERROR = '00000';
  const PENDING_CODE = '99999';

  /**
   * The Commerce Payment Storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentStorage;

  /**
   * The Commerce Order Type Storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderTypeStorage;

  /**
   * Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface Service.
   *
   * @var \Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface
   */
  protected $pbxCmdRefHelper;

  /**
   * Price rounder service.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $priceRounder;

  /**
   * PaymentController constructor.
   */
  public function __construct(PbxCmdRefHelperInterface $pbxCmdRefHelper, RounderInterface $priceRounder) {
    $this->paymentStorage = $this->entityTypeManager()->getStorage('commerce_payment');
    $this->orderTypeStorage = $this->entityTypeManager()->getStorage('commerce_order_type');
    $this->pbxCmdRefHelper = $pbxCmdRefHelper;
    $this->priceRounder = $priceRounder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_paybox_payment.pbx_cmd_ref_helper'),
      $container->get('commerce_price.rounder'),
    );
  }

  /**
   * Method called by Paybox Server on payment return.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Basic Json Response.
   */
  public function return(Request $request) {
    $paymentId = $this->pbxCmdRefHelper->extractPaymentIdFromRef($request);
    $returnAmount = $request->query->get('Mt');
    $transactionId = $request->query->get('Transaction');
    $appelId = $request->query->get('Appel');
    $transactionDate = $request->query->get('Date');
    $error = $request->query->get('Error');
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = $this->paymentStorage->load($paymentId);

    $paymentAmount = (float) $payment->getAmount()->getNumber() * 100;
    $paymentAmount = str_pad((string) $paymentAmount, 3, '0', STR_PAD_LEFT);
    if (($returnAmount !== $paymentAmount) || (!empty($error) && self::EMPTY_ERROR !== $error  && self::PENDING_CODE !== $error)) {

      if (self::EMPTY_ERROR === $error) {
        $error = 'Charged amount not equal to order amount';
      }
      $this->getLogger('commerce_paybox_payment')
        ->error('Error during api return paybox on payment @payment error @error', [
          '@payment' => $payment->id(),
          '@error' => $error,
        ]);

      $payment->setState('failure');
      $payment->set('remote_state', $error);
      $payment->save();
      return new JsonResponse(FALSE);
    }

    if (self::PENDING_CODE === $error) {
      $payment->setState('pending');
      $payment->save();

      return new JsonResponse(TRUE);
    }

    $payment->setState('completed');
    $payment->set('remote_id', Json::encode([
      'TRANS' => $transactionId,
      'APPEL' => $appelId,
    ]));
    $payment->set('authorized', strtotime('today ' . $transactionDate));
    $payment->save();

    // Validate and unlock order.
    $order = $payment->getOrder();
    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $orderType */
    $orderType = $this->orderTypeStorage->load($order->bundle());
    switch ($orderType->getWorkflowId()) {
      case 'order_fulfillment':
      case 'order_fulfillment_validation':
        $order->set('state', 'fulfillment');
        break;

      case 'order_default_validation':
      case 'order_default':
      default:
        $order->set('state', 'completed');
        break;
    }
    $order->unlock();
    $order->save();

    return new JsonResponse(TRUE);
  }

  /**
   * BuildAdminPayboxRedirectForm documentation.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The Commerce Order entity.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $commerce_payment
   *   The Commerce Payment entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   */
  public function buildAdminPayboxRedirectForm(OrderInterface $commerce_order, PaymentInterface $commerce_payment, Request $request) {

    // Redefine payment amount if it's been updated while making payment.
    // Only if new amount is smaller than previous, we don't want to pay more.
    if ($new_amount = $request->get('amount')) {
      $oldAmount = $commerce_payment->getAmount();
      $newAmount = Price::fromArray([
        'number' => str_replace(',', '.', $new_amount),
        'currency_code' => $oldAmount->getCurrencyCode(),
      ]);
      if ($newAmount->compareTo($oldAmount) === -1) {
        $commerce_payment->setAmount($newAmount);
        $commerce_payment->save();
      };
    }

    // Check for payment gateway.
    /** @var \Drupal\commerce_paybox_payment\Plugin\Commerce\PaymentGateway\Paybox $payment_gateway_plugin */
    $payment_gateway_plugin = $commerce_payment->getPaymentGateway()->getPlugin();
    if ($payment_gateway_plugin->getPluginId() !== 'paybox') {
      $this->messenger()->addError($this->t("Invalid payment gateway: @payment_gateway_plugin", [
        '@payment_gateway_plugin' => $payment_gateway_plugin->getPluginId(),
      ]));
      return [];
    }

    // Check for payment state.
    if ($commerce_payment->getState()->getId() !== 'new') {
      $this->messenger()->addError($this->t("Invalid payment state: @state", [
        '@state' => $commerce_payment->getState()->getId(),
      ]));
    }

    // Generate data using current $payment, $order and payment gateway config.
    $plugin_config = $payment_gateway_plugin->getConfiguration();
    $data = $payment_gateway_plugin->request($commerce_order, $commerce_payment);

    $url_params = [
      'commerce_order' => $commerce_order->id(),
      'commerce_payment' => $commerce_payment->id(),
    ];
    $return_url = Url::fromRoute('commerce_paybox_payment.add_payment_return', $url_params)->setAbsolute(TRUE)->toString();
    $cancel_url = Url::fromRoute('commerce_paybox_payment.add_payment_cancel', $url_params)->setAbsolute(TRUE)->toString();
    $ipn_url = Url::fromRoute('commerce_paybox_payment.payment_api_return', $url_params)->setAbsolute(TRUE)->toString();
    $data['PBX_EFFECTUE'] = $return_url;
    $data['PBX_ATTENTE'] = $return_url;
    $data['PBX_REPONDRE_A'] = $ipn_url;
    $data['PBX_ANNULE'] = $cancel_url;
    $data['PBX_REFUSE'] = $cancel_url;
    $data['PBX_HMAC'] = $this->getDataHmac($data, $plugin_config['secret_key']);

    // Render a message to inform user which will get out Drupal site.
    $paidAmount = $commerce_payment->getAmount();
    $roundedAmount = $this->priceRounder->round($paidAmount);
    $this->messenger()->addStatus($this->t('This button will lead you out of the Drupal site to make a <strong>@amount @currency</strong> payment.', [
      '@currency' => $roundedAmount->getCurrencyCode(),
      '@amount' => $roundedAmount->getNumber(),
    ]));

    return [
      '#theme' => 'paybox_payment_admin_redirect_form',
      '#action' => $plugin_config['server_url'],
      '#data' => $data,
    ];
  }

  /**
   * AddPaymentReturn documentation.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The Commerce Order entity.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $commerce_payment
   *   The Commerce Payment entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   */
  public function addPaymentReturn(OrderInterface $commerce_order, PaymentInterface $commerce_payment, Request $request) {

    // Use Paybox gateway plugin method to handle payment return.
    /** @var \Drupal\commerce_paybox_payment\Plugin\Commerce\PaymentGateway\Paybox $payment_gateway_plugin */
    $payment_gateway_plugin = $commerce_payment->getPaymentGateway()->getPlugin();
    $payment_gateway_plugin->onReturn($commerce_order, $request);

    // Redirect to order payments page.
    return $this->redirect('entity.commerce_payment.collection', ['commerce_order' => $commerce_order->id()]);
  }

  /**
   * AddPaymentCancel documentation.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The Commerce Order entity.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $commerce_payment
   *   The Commerce Payment entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   */
  public function addPaymentCancel(OrderInterface $commerce_order, PaymentInterface $commerce_payment, Request $request) {
    // Use Paybox gateway plugin method to handle payment cancellation.
    /** @var \Drupal\commerce_paybox_payment\Plugin\Commerce\PaymentGateway\Paybox $payment_gateway_plugin */
    $payment_gateway_plugin = $commerce_payment->getPaymentGateway()->getPlugin();
    $payment_gateway_plugin->onCancel($commerce_order, $request);

    // Redirect to order payments page.
    return $this->redirect('entity.commerce_payment.collection', ['commerce_order' => $commerce_order->id()]);
  }

  /**
   * Get Hmac hash from array request.
   *
   * @param array $data
   *   Request data.
   * @param string $secretKey
   *   Secret key.
   *
   * @return string
   *   Hmac data request.
   */
  private function getDataHmac(array $data, string $secretKey) {
    // Converts the array into a string of key=value.
    $attached_data = [];
    foreach ($data as $key => $value) {
      $attached_data[] = $key . '=' . $value;
    }

    $msg = implode('&', $attached_data);
    $binKey = pack("H*", $secretKey);

    return strtoupper(hash_hmac('sha512', $msg, $binKey));
  }

}
