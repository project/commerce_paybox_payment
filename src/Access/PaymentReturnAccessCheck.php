<?php

namespace Drupal\commerce_paybox_payment\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface;
use Drupal\commerce_paybox_payment\SignatureChecker;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Custom Access check service.
 */
class PaymentReturnAccessCheck implements AccessInterface {

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  private $request;

  /**
   * Commerce Payment Storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $paymentStorage;

  /**
   * Commerce Payment Gateway Storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $gatewayStorage;

  /**
   * Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface Service.
   *
   * @var \Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface
   */
  private $pbxCmdRefHelper;

  /**
   * PaymentReturnAccessCheck constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request Stack Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   EntityTypeManager Service.
   * @param \Drupal\commerce_paybox_payment\Services\PbxCmdRefHelperInterface $pbxCmdRefHelper
   *   PbxCmdRefHelperInterface Service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    RequestStack $requestStack,
    EntityTypeManagerInterface $entityTypeManager,
    PbxCmdRefHelperInterface $pbxCmdRefHelper) {
    $this->request = $requestStack->getCurrentRequest();
    $this->paymentStorage = $entityTypeManager->getStorage('commerce_payment');
    $this->gatewayStorage = $entityTypeManager->getStorage('commerce_payment_gateway');
    $this->pbxCmdRefHelper = $pbxCmdRefHelper;
  }

  /**
   * Manage access payment return.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current User.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Result Access.
   */
  public function access(AccountInterface $account) {
    if ($account->isAuthenticated()) {
      return AccessResult::forbidden();
    }

    foreach (['Ref', 'Mt', 'Signature', 'Error'] as $param) {
      if (empty($this->request->query->get($param))) {
        return AccessResult::forbidden();
      }
    }
    $paymentId = $this->pbxCmdRefHelper->extractPaymentIdFromRef($this->request);
    $payment = $this->paymentStorage->load($paymentId);
    if (!$payment instanceof PaymentInterface) {
      return AccessResult::forbidden();
    }

    if ('completed' === $payment->getState()->getId()) {
      return AccessResult::forbidden();
    }

    if (1 !== SignatureChecker::checkSignature($this->request)) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

}
