<?php

namespace Drupal\commerce_paybox_payment;

use Symfony\Component\HttpFoundation\Request;

/**
 * Signature Checker class.
 */
class SignatureChecker {

  /**
   * Check payment signature to restrict return access.
   *
   * @return int
   *   Status on check.
   */
  public static function checkSignature(Request $request) {
    $key = self::loadPublicKey();
    if (!$key) {
      return -1;
    }
    $data = self::getRequestData($request);
    $sig = $request->query->get('Signature');
    $sig = base64_decode($sig, TRUE);

    return openssl_verify($data, $sig, $key);
  }

  /**
   * Concatenate request into string.
   *
   * @return string
   *   Converted string request.
   */
  private static function getRequestData(Request $request) {
    // Converts the array into a string of key=value.
    $args = [];
    foreach ($request->query->all() as $key => $value) {
      if ('Signature' === $key) {
        continue;
      }
      $args[] = $key . '=' . urlencode($value);
    }

    return implode('&', $args);
  }

  /**
   * Get public key openssl.
   *
   * @return false|resource
   *   Public Key if exist or false otherwise.
   */
  private static function loadPublicKey() {
    $keyFile = \Drupal::service('extension.list.module')->getPath('commerce_paybox_payment') . '/pubkey.pem';
    $fileSize = filesize($keyFile);

    if (!$fileSize) {
      return FALSE;
    }
    $stream = fopen($keyFile, 'r');
    $fileData = fread($stream, $fileSize);
    fclose($stream);

    if (!$fileData) {
      return FALSE;
    }

    return openssl_pkey_get_public($fileData);
  }

}
